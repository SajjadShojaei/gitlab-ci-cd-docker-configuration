######################################################################################
# Stages
**Understanding stages**

In GitLab CI/CD, you use stages to group jobs based on the development workflow and control the order of execution for CI/CD jobs.

> Pipelines execute each stage in order, where all jobs in a single stage run in parallel. After a stage completes, the pipeline moves on to execute the next stage and runs those jobs, and the process continues like this until the pipeline completes or a job fails. If a job fails, the jobs in later stages don't start at all.

Use stages to define stages that contain groups of jobs. Use stage in a job to configure the job to run in a specific stage.

	stages:
  		- git_pull                        # stage 1
  		- install_dependencies            # stage 2
  		- docker-compose-dev              # stage 3
  		- docker-compose-prod             # stage 4

######################################################################################
# Variables
	variables:
  		PROJECT_DIR: $CI_BUILD_DIR  # or custome directory
  		CONTAINER_DEV: container_name_dev
  		CONTAINER_PROD: container_name_prod
  		IMAGE_PROD: image_name_prod:latest


######################################################################################
# Git pull 
--> pull latest changes from git

In stage 1 first at all we `pull` last code change to project.

	Git_pull: 				# stage 1	
  	stage: git_pull       
  	script:
    	  - cd $PROJECT_DIR    	# cd to the project root                      
    	  - git pull                      	# pull the latest code

######################################################################################
# Install dependencies
--> install dependencies

In stage 2 we install dependencies, but this stage only run if `package-lock.json` has changed.
 
	Install_dependencies: 			# stage 2
  	stage: install_dependencies 
  	script:
    	  - cd $PROJECT_DIR 		# cd to the project root                   
    	  - npm ci                  	# install dependencies
  	only:
    	changes:
      	  - package-lock.json           		# only run if package-lock.json has changed

######################################################################################
# Docker compose as development build

In stage 3 we want to build and run the `docker-compose` as `development`.
But we also make several conditions whether the container is running or not and then stop them.

        Docker_run_dev:
        stage: docker-compose-dev 			# stage 3       
        only:
         - main 						# only run on main branch                     
        script:
          - cd $PROJECT_DIR 			# cd to the project root               
          - |  
            if [ "$(docker ps -qa -f name=$CONTAINER_DEV)" ]; then
               echo ":: Found container - $container_name_dev"
               if [ "$(docker ps -q -f name=$CONTAINER_DEV)" ]; then
                  echo ":: Stopping running container - $CONTAINER_DEV"
                  docker stop $CONTAINER_DEV
               fi
               if [ "$(docker ps -q -f name=$CONTAINER_PROD)" ]; then
                echo ":: Stopping running container - $CONTAINER_PROD"
                docker stop $CONTAINER_PROD
               fi
             else
                if [ "$(docker ps -qa -f name=$CONTAINER_PROD)" ]; then
                   echo ":: Found container - $CONTAINER_PROD"
                   if [ "$(docker ps -q -f name=$CONTAINER_PROD)" ]; then
                      echo ":: Stopping running container - $CONTAINER_PROD"
                      docker stop $CONTAINER_PROD
                   fi
                   echo ":: Stopping container - $CONTAINER_PROD"
                 fi   
                 echo ":: No container found "  
                fi 
          - rm -rf dist/     				# remove dist folder                              
          - docker-compose up -d app                	# run the docker-compose as development build         
        
Addition explain:

     if [ "$(docker ps -qa -f name=$CONTAINER_DEV)" ]; then

This is code, Specifies whether the $CONTAINER_DEV container ID exists among all containers. It then returns the message "Found container - $CONTAINER_DEV".

     if [ "$(docker ps -q -f name=$CONTAINER_DEV)" ]; then

This code, Specifies whether the $CONTAINER_DEV container ID is present among running containers. Because both containers can not run on one port. It then stops the container.

######################################################################################
# Docker compose as production build

In next step, we want to build and execute `docker-compose` as `production`.
But we also make several conditions whether the container works or not and then stop them.
After stopping the running containers, we remove the containers to rebuild them.

  	Docker_run_prod:
    	stage: docker-compose-prod        # stage 4
    	only:
     	 - master                          # only run on master branch
    	script:
      	  - cd $PROJECT_DIR/dist/src                           # cd to the project root and to the dist/src folder
      	  - cp -r ./ ../                                                    # copy the dist/src folder to the project root
      	  - cd ../../                                                       # cd to the project root
     	  - |   
      	  if [ "$(docker ps -qa -f name=$CONTAINER_PROD)" ]; then
       	    echo ":: Found container - $CONTAINER_PROD"
       	    if [ "$(docker ps -q -f name=$CONTAINER_PROD)" ]; then
        	echo ":: Stopping running container - $CONTAINER_PROD"
        	docker stop $CONTAINER_PROD
       	    fi
       	    else
        	if [ "$(docker ps -qa -f name=$CONTAINER_DEV)" ]; then
         	    echo ":: Found container - $CONTAINER_DEV"
         	    if [ "$(docker ps -q -f name=$CONTAINER_DEV)" ]; then
         	        echo ":: Stopping running container - $CONTAINER_DEV"
         	        docker stop $CONTAINER_DEV
        	    fi
        	    echo ":: Stopping container - $CONTAINER_DEV"
       	   	fi   
      	    	echo ":: No container found "  
      	  	fi
     	   - docker rm -f  $CONTAINER_PROD                         # remove the prod container
    	   - docker rmi -f  $IMAGE_PROD                  		   # remove the prod image
    	   - |
    	     if [ "$(docker images -f "dangling=true" -q )" ]; then
    	         docker rmi $(docker images --filter "dangling=true" -q --no-trunc)
    	         echo "Removed dangling images"
    	    fi
   	    - docker-compose build prod                                      # build the docker-compose as production         
   	    - docker-compose up -d prod                                      # run the docker-compose as production build
   	    
Addition explain:

     cd $PROJECT_DIR/dist/src
     cp -r ./ ../
     cd ../../

When we run `npm run start:dev` it automatically creates the `dist` file which contains the `src` folder and all the necessary files are in it. 
With the command `cp -r ./ ../` Copy all the files in the `src` folder to a previous folder, ie `dist`.
Finally, we return to the main path of the project with the cd command `cd ../../`.

     if [ "$(docker ps -qa -f name=container_name_prod)" ]; then

This is code, Specifies whether the container_name_prod container ID exists among all containers, Like stage 3. It then returns the message "Found container - container_name_prod".

     if [ "$(docker ps -q -f name=container_name_prod)" ]; then

This code, Specifies whether the container_name_prod container ID is present among running containers. Like stage 3. Because both containers can not run on one port. It then stops the container.   

```
if [ "$(docker images -f "dangling=true" -q )" ]; then
	         docker rmi $(docker images --filter "dangling=true" -q --no-trunc)
	         echo "Removed dangling images"
fi
```
In this code, the ID filters all images that are "dangling = true" and deletes them.

```
docker-compose build prod
docker-compose up -d prod
```
Build docker-compose as production and run it.
