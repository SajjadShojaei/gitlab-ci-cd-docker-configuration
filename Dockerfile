FROM node:12.19.0-alpine3.9 AS development

# Create app directory and copy source code into it
RUN mkdir -p /app

COPY . /app

WORKDIR /app

COPY ./package.json ./package-lock.json /app/

RUN npm install --only=development

COPY . .

ENV PORT 6000

EXPOSE 6000

CMD ["npm", "run", "build"]


FROM node:12.19.0-alpine3.9 AS production

# Create app directory and copy source code into it
RUN mkdir -p /app

COPY . /app

WORKDIR /app

COPY ./package.json ./package-lock.json /app/

RUN npm install

COPY . .

COPY --from=development /app/dist ./dist

ENV PORT 6000

EXPOSE 6000

CMD ["node", "dist/main"]
